import 'dart:math';

import 'package:club_house_ui/data.dart';
import 'package:club_house_ui/models/room.dart';
import 'package:club_house_ui/screens/profile_screen.dart';
import 'package:club_house_ui/widgets/room_user_profile.dart';
import 'package:club_house_ui/widgets/user_profile_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RoomScreen extends StatelessWidget {
  final Room room;

  const RoomScreen({super.key, required this.room});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leadingWidth: 150,
        leading: TextButton.icon(
          onPressed: () => Navigator.of(context).pop(),
          style: TextButton.styleFrom(primary: Colors.black),
          icon: const Icon(
            Icons.keyboard_arrow_down,
            size: 28,
          ),
          label: const Text(
            'All rooms',
            style: TextStyle(fontSize: 16),
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(
              CupertinoIcons.doc,
              size: 25,
            ),
          ),
          SizedBox(
            width: 20,
          ),
          GestureDetector(
            child: UserProfileImage(
              imageUrl: currentUser.profileImage,
              size: 35,
            ),
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                fullscreenDialog: true,
                builder: (_) => ProfileScreen(),
              ),
            ),
          ),
          SizedBox(
            width: 15,
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
        ),
        child: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "${room.club} 🏠".toUpperCase(),
                        style: Theme.of(context).textTheme.overline!.copyWith(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1,
                            ),
                      ),
                      Icon(
                        CupertinoIcons.ellipsis,
                      ),
                    ],
                  ),
                  Text(
                    room.name,
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 1,
                        ),
                  ),
                ],
              ),
            ),
            SliverPadding(
              padding: const EdgeInsets.all(8.0),
              sliver: SliverGrid.count(
                crossAxisCount: 3,
                mainAxisSpacing: 20,
                children: [
                  ...room.speakers.map(
                    (e) => RoomUserProfile(
                      imgUrl: e.profileImage,
                      name: e.firstName,
                      size: 80,
                      isNew: Random().nextBool(),
                      isMuted: Random().nextBool(),
                    ),
                  ),
                ],
              ),
            ),
            SliverToBoxAdapter(
              child: Text(
                'Followed by speakers',
                style: Theme.of(context).textTheme.subtitle2!.copyWith(
                      fontSize: 14,
                      color: Colors.grey,
                      fontWeight: FontWeight.bold,
                    ),
              ),
            ),
            SliverPadding(
              padding: const EdgeInsets.all(8),
              sliver: SliverGrid.count(
                crossAxisCount: 4,
                childAspectRatio: 0.8,
                children: [
                  ...room.followedBySpeakers.map(
                    (e) => RoomUserProfile(
                      imgUrl: e.profileImage,
                      name: e.firstName,
                      size: 60,
                      isNew: Random().nextBool(),
                      isMuted: false,
                    ),
                  ),
                ],
              ),
            ),
            SliverToBoxAdapter(
              child: Text(
                'Others in the room',
                style: Theme.of(context).textTheme.subtitle2!.copyWith(
                      fontSize: 14,
                      color: Colors.grey,
                      fontWeight: FontWeight.bold,
                    ),
              ),
            ),
            SliverPadding(
              padding: const EdgeInsets.all(8),
              sliver: SliverGrid.count(
                crossAxisCount: 4,
                childAspectRatio: 0.8,
                children: [
                  ...room.others.map(
                    (e) => RoomUserProfile(
                      imgUrl: e.profileImage,
                      name: e.firstName,
                      size: 60,
                      isNew: Random().nextBool(),
                      isMuted: false,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        margin: EdgeInsets.only(bottom: 20),
      ),
      bottomSheet: Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(color: Colors.white),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ElevatedButton.icon(
              onPressed: () {},
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.grey[100],
              ),
              icon: Icon(
                Icons.waving_hand,
                color: Colors.orange,
              ),
              label: Text(
                'Leave quietly',
                style: TextStyle(
                  color: Colors.red,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Row(
              children: [
                IconButton(
                  onPressed: () {},
                  style: IconButton.styleFrom(
                    backgroundColor: Colors.grey[100],
                  ),
                  icon: Icon(
                    Icons.add,
                    color: Colors.black,
                    size: 26,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  style: IconButton.styleFrom(
                    backgroundColor: Colors.grey[100],
                  ),
                  icon: Icon(
                    Icons.front_hand_outlined,
                    color: Colors.grey[400],
                    size: 26,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
