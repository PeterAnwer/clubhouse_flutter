import 'package:flutter/material.dart';

const Color kBackgroundColor = Color(0xFFF4EEE6);
const Color kAccentColor = Color(0xFF28AB60);