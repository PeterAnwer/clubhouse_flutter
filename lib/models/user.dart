class User {
  final String firstName;
  final String lastName;
  final String profileImage;
  final bool isNew;
  final bool isMuted;

  const User({
    required this.firstName,
    required this.lastName,
    required this.profileImage,
    this.isNew = true,
    this.isMuted = true,
  });
}
