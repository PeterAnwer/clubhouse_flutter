import 'package:club_house_ui/constants.dart';
import 'package:club_house_ui/screens/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// theme-main-color = #F4EEE6;
// theme-sec-color = #28AB60;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Clubhouse UI",
      theme: ThemeData(
        appBarTheme: AppBarTheme(backgroundColor: kBackgroundColor),
        scaffoldBackgroundColor: kBackgroundColor,
        primaryColor: Colors.white,
        secondaryHeaderColor: kAccentColor,
        // accentColor: Color(kAccentColor),
        iconTheme: IconThemeData(color: Colors.black),
        fontFamily: GoogleFonts.montserrat().fontFamily,
        textTheme: GoogleFonts.montserratTextTheme(),
      ).copyWith(
        colorScheme:
            ThemeData().colorScheme.copyWith(secondary: kAccentColor),
      ),
      home: HomeScreen()
    );
  }
}
