import 'package:club_house_ui/models/room.dart';
import 'package:club_house_ui/screens/room_screen.dart';
import 'package:club_house_ui/widgets/user_profile_image.dart';
import 'package:flutter/material.dart';

class RoomCard extends StatelessWidget {
  final Room room;

  const RoomCard({super.key, required this.room});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          fullscreenDialog: true,
          builder: (_) => RoomScreen(room: room),
        ),
      ),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "${room.club} 🏠".toUpperCase(),
                style: Theme.of(context).textTheme.overline!.copyWith(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      letterSpacing: 1,
                    ),
              ),
              Text(
                room.name,
                style: Theme.of(context).textTheme.overline!.copyWith(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        child: Stack(
                          children: [
                            UserProfileImage(
                              imageUrl: room.speakers[0].profileImage,
                              size: 48,
                            ),
                            Positioned(
                              left: 24,
                              top: 24,
                              child: UserProfileImage(
                                imageUrl: room.speakers[1].profileImage,
                                size: 48,
                              ),
                            ),
                          ],
                        ),
                        height: 100,
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ...room.speakers.map(
                              (e) => Text(
                                "${e.firstName} ${e.lastName} 💬",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1!
                                    .copyWith(
                                      fontSize: 16,
                                    ),
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  TextSpan(
                                    text:
                                        "${room.speakers.length + room.followedBySpeakers.length + room.others.length} ",
                                  ),
                                  WidgetSpan(
                                    child: Icon(
                                      Icons.person,
                                      size: 18,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  TextSpan(
                                    text: " /  ${room.speakers.length}  ",
                                  ),
                                  WidgetSpan(
                                    child: Icon(
                                      Icons.chat_outlined,
                                      size: 18,
                                      color: Colors.grey,
                                    ),
                                  ),
                                ],
                              ),
                              style: TextStyle(
                                color: Colors.grey[700],
                              ),
                            ),
                          ],
                        ),
                      ),
                      flex: 2,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
