import 'package:club_house_ui/widgets/user_profile_image.dart';
import 'package:flutter/material.dart';

class RoomUserProfile extends StatelessWidget {
  final String imgUrl;
  final String name;
  final double size;
  final bool isNew;
  final bool isMuted;

  const RoomUserProfile({
    super.key,
    required this.imgUrl,
    required this.name,
    this.size = 42,
    this.isNew = true,
    this.isMuted = true,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Stack(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: UserProfileImage(
                imageUrl: imgUrl,
                size: size,
              ),
            ),
            if (isNew)
              Positioned(
                bottom: 0,
                left: 0,
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black,
                        offset: Offset(0, 2),
                        blurRadius: 4,
                      ),
                    ],
                  ),
                  child: Padding(
                      padding: const EdgeInsets.all(4),
                      child: Text(
                        '🎉',
                        style: TextStyle(fontSize: size / 4),
                      )),
                ),
              ),
            if (isMuted)
              Positioned(
                bottom: 0,
                right: 0,
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black,
                        offset: Offset(0, 2),
                        blurRadius: 4,
                      ),
                    ],
                  ),
                  child: Padding(
                      padding: const EdgeInsets.all(4),
                      child: Icon(
                        Icons.mic_off,
                        size: size / 3,
                      )),
                ),
              ),
          ],
        ),
        Flexible(
          child: Text(
            name,
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ],
    );
  }
}
